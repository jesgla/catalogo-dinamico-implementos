import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCuentaComponent } from './page-cuenta.component';

describe('PageCuentaComponent', () => {
  let component: PageCuentaComponent;
  let fixture: ComponentFixture<PageCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
