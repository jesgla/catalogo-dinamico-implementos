import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiCuentaRoutingModule } from './mi-cuenta-routing.module';
import { PageCuentaComponent } from './pages/page-cuenta/page-cuenta.component';



@NgModule({
  declarations: [PageCuentaComponent],
  imports: [
    CommonModule,
    MiCuentaRoutingModule
  ]
})
export class MiCuentaModule { }
