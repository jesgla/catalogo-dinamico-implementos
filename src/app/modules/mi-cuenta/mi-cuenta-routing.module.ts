import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageCuentaComponent } from './pages/page-cuenta/page-cuenta.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'perfil'
  },
  {
    path: 'perfil',
    component: PageCuentaComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MiCuentaRoutingModule { }
