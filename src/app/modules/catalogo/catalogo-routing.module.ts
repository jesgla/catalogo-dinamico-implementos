import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageCatalogoComponent } from './pages/page-catalogo/page-catalogo.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'ver-catalogos'
  },
  {
    path: 'ver-catalogos',
    component: PageCatalogoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogoRoutingModule { }
