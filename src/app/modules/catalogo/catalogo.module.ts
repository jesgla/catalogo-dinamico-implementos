import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogoRoutingModule } from './catalogo-routing.module';
import { PageCatalogoComponent } from './pages/page-catalogo/page-catalogo.component';


@NgModule({
  declarations: [PageCatalogoComponent],
  imports: [
    CommonModule,
    CatalogoRoutingModule
  ],
  exports:[
    PageCatalogoComponent
  ]
})
export class CatalogoModule { }
