import { Component, OnInit, AfterViewInit } from '@angular/core';
import { trigger, state, transition, animate, style } from '@angular/animations';

import { Router } from '@angular/router';
import { SiderbarService } from '../../services/siderbar.service';


@Component({
  selector: 'app-siderbar',
  templateUrl: './siderbar.component.html',
  styleUrls: ['./siderbar.component.scss'],
  animations: [
    trigger('slide', [
      state('up', style({ height: 0 })),
      state('down', style({ height: '*' })),
      transition('up <=> down', animate(200))
    ])
  ]
})
export class SiderbarComponent implements OnInit,AfterViewInit{

  menus = [];
  usuario: any;
  constructor(
    public sidebarservice: SiderbarService,
    private router: Router,
    ) {
    this.menus = sidebarservice.getMenuList();
    this.usuario='';
   }
 
   async ngOnInit() {
      
   }
   ngAfterViewInit(){
  
   } 
  toggleSidebar() {
    this.sidebarservice.setSidebarState(!this.sidebarservice.getSidebarState());
  }
  getSideBarState() {
    return this.sidebarservice.getSidebarState();
  }

  toggle(currentMenu) {
    if (currentMenu.type === 'dropdown') {
      this.menus.forEach(element => {
        if (element === currentMenu) {
          currentMenu.active = !currentMenu.active;
        } else {
          element.active = false;
        }
      });
    }
  }

  getState(currentMenu) {

    if (currentMenu.active) {
      return 'down';
    } else {
      return 'up';
    }
  }

  hasBackgroundImage() {
    return this.sidebarservice.hasBackgroundImage;
  }
  logout() {
    this.usuario='';

    this.router.navigate(['/login']);
}
}

