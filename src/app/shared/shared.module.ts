import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiderbarComponent } from './components/siderbar/siderbar.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SiderbarComponent,
    NavBarComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    PerfectScrollbarModule,
    BrowserAnimationsModule
  ],
  exports:[
    SiderbarComponent,
    NavBarComponent
  ]
})
export class SharedModule { }
