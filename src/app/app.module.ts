import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//modules
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

//components
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { CatalogoModule } from './modules/catalogo/catalogo.module';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PageHomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CatalogoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
