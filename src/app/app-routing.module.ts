import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { PageCatalogoComponent } from './modules/catalogo/pages/page-catalogo/page-catalogo.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'inicio'
  },
  {
    path: 'inicio',
    component: DashboardComponent,

    children: [
      {
        path: '',
        component: PageHomeComponent,

      },
      {
        path: '**',
        component: PageHomeComponent
      }
    ],
  },
  {
    path: 'mi-cuenta',
    component: DashboardComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/mi-cuenta/mi-cuenta.module').then(m => m.MiCuentaModule)
      },
    ],
  },
  {
    path: 'catalogo',
    component: DashboardComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/catalogo/catalogo.module').then(m => m.CatalogoModule)
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'inicio'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
